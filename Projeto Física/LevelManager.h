#pragma once
#include <iostream>
#include <fstream>
#include <Box2D\Box2D.h>
#include <string>
#include <GL\glut.h>
#include <list>
#include <stdio.h>
using namespace std;

class LevelManager
{
	struct Position
	{
		float pos_x;
		float pos_y;
	};
	struct Shapes
	{
		float half_width;
		float half_height;
	};
	struct Color
	{
		int r, g, b;
	};
	struct Ground
	{
		Color color;
		b2Body *body;
		b2BodyDef bodyDef;
		b2PolygonShape polygonShape;
		float width, height;
	};
	struct Player
	{
		Color color;
		b2Body *body;
		b2BodyDef bodyDef;
		b2PolygonShape polygonShape;
		b2FixtureDef fixtureDef;
		float width, height;
		float density, friction;
	};
public:
	LevelManager();
	~LevelManager();
	void Initializer();
	void Drawn();
	void Player_Drawn();
	void Invert_Gravity();
	void Move(int direction);
	b2World *world;
	Player player;
private:
	void SetGroundDefinitions(float x, float y, float half_width, float half_height, int r, int g, int b);
	void SetPlayerDefinitions();
	void Set_Level();
	void ResetGravity();
	void Readfile(string filename);
	float g;
	b2Vec2 gravity;	
	list<Ground> list_ground;
	b2Vec2 right;
	b2Vec2 left;
	b2Vec2 start_pos;
};

