#pragma once
#include <iostream>
#include "LevelManager.h"
#include <Box2D\Box2D.h>
#include <GL\glut.h>
#include <stdio.h>
using namespace std;
class GameManager
{
	enum MyEnum
	{
		right, left, space
	};
public:
	GameManager();
	~GameManager();
public:
	void Initializer();
	void Drawn();
	void Simulate();
	LevelManager &returnLevelManager(){ return levelmanager; };
private:
	LevelManager levelmanager;
	float timeStep;
	int32 velocityIterations;
	int32 positionIterations;
};

