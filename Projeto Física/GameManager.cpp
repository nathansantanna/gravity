#include "GameManager.h"


GameManager::GameManager()
{
	timeStep = 1.0f / 60.0f;
	velocityIterations = 6;
	positionIterations = 2;
}


GameManager::~GameManager()
{
}

void GameManager::Initializer()
{
	levelmanager.Initializer();
}

void GameManager::Drawn()
{
	levelmanager.Drawn();
	levelmanager.Player_Drawn();
}

void GameManager::Simulate()
{
	for (int32 i = 0; i < 60; i++)
	{
		levelmanager.world->Step(timeStep, velocityIterations, positionIterations);
		b2Vec2 position = levelmanager.player.body->GetPosition();
		float32 angle = levelmanager.player.body->GetAngle();
		//printf("%4.2f %4.2f %4.2f\n", levelmanager.player.body->GetPosition().x, levelmanager.player.body->GetPosition().y, angle);
	}
}