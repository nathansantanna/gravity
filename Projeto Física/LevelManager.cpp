#include "LevelManager.h"


LevelManager::LevelManager()
{
	g = -0.3f;
	gravity.Set(0.0f, g);	world = new b2World(gravity);	player.width = 2;
	player.height = 2;
	player.density = 1.0f;
	player.friction = 0.3f;
	player.color.r = 0;
	player.color.g = 0;
	player.color.b = 1;
}

//void levelmanager::readfile(string filename)
//{
//	ifstream abrir(filename);
//	if (!abrir.is_open())
//	{
//		cout << "impossible to open" << endl;
//		exit(true);
//	}
//	for (int i = 0; i < abrir.eof(); i++)
//	{
//		ground new_ground;
//		position p;
//		shapes s;
//		abrir >> p.pos_x >> p.pos_y >> s.half_width >> s.half_height >> new_ground.color.r >> new_ground.color.g >> new_ground.color.b;
//		new_ground.bodydef.position.set(p.pos_x, p.pos_y);
//		new_ground.polygonshape.setasbox(s.half_width, s.half_height);
//		new_ground.height = s.half_height * 2;
//		new_ground.width = s.half_width * 2;
//		list_ground.push_back(new_ground);
//		cout << p.pos_x;
//	}
//}

LevelManager::~LevelManager()
{
}

void LevelManager::Initializer()
{
	
	right = b2Vec2(0.6f, 0.0f);
	left = b2Vec2(-0.6f, 0.0f);
	start_pos = b2Vec2(0.0f, 5.0f);
	
	SetGroundDefinitions(0, -5, 50, 5, 1, 0, 0);
	SetGroundDefinitions(40, 10, 50, 5, 1, 0, 0);
	SetGroundDefinitions(85, -5, 50, 5, 1, 0, 0);
	SetGroundDefinitions(85, -5, 5, 50, 0, 1, 0);
	SetGroundDefinitions(85, 30, 25, 5, 1, 1, 0);
	SetGroundDefinitions(115, -20, 15, 5, 1, 1, 0);
	SetGroundDefinitions(85, -50, 7, 5, 1, 0, 1);

	SetPlayerDefinitions();
	Set_Level();
}

void LevelManager::Set_Level()
{
	for (list<Ground>::iterator it = list_ground.begin(); it != list_ground.end(); it++)
	{
		it->body = world->CreateBody(&it->bodyDef);
		it->body -> CreateFixture(&it -> polygonShape, 0.0f);
	}
}

void LevelManager::Drawn()
{
	for (list<Ground>::iterator it = list_ground.begin(); it != list_ground.end(); it++)
	{
		glColor3f(it->color.r, it->color.g, it->color.b);
		glBegin(GL_POLYGON);
		glVertex3f(it->body->GetPosition().x - it->width / 2, it->body->GetPosition().y + it->height / 2, 0.0); // -x, y
		glVertex3f(it->body->GetPosition().x + it->width / 2, it->body->GetPosition().y + it->height / 2, 0.0); // x, y
		glVertex3f(it->body->GetPosition().x + it->width / 2, it->body->GetPosition().y - it->height / 2, 0.0); // x, -y
		glVertex3f(it->body->GetPosition().x - it->width / 2, it->body->GetPosition().y - it->height / 2, 0.0); // -x, -y
		glEnd();
	}
}

void LevelManager::Player_Drawn()
{
	glColor3f(player.color.r, player.color.g, player.color.b);
	glBegin(GL_POLYGON);
	glVertex3f(player.body->GetPosition().x - player.width / 2, player.body->GetPosition().y + player.height / 2, 0.0f);
	glVertex3f(player.body->GetPosition().x + player.width / 2, player.body->GetPosition().y + player.height / 2, 0.0f);
	glVertex3f(player.body->GetPosition().x + player.width / 2, player.body->GetPosition().y - player.height / 2, 0.0f);
	glVertex3f(player.body->GetPosition().x - player.width / 2, player.body->GetPosition().y - player.height / 2, 0.0f);
	glEnd();

}

void LevelManager::Invert_Gravity()
{
	g = -g;
	gravity.Set(0.0f, g);
	world->SetGravity(gravity);
}


void LevelManager::ResetGravity()
{
	if (g == 0.3f)
	{
		g = -0.3f;
		gravity.Set(0.0f, g);
		world->SetGravity(gravity);
	}
}



void LevelManager::SetPlayerDefinitions()
{
	player.bodyDef.type = b2_dynamicBody;
	player.bodyDef.position.Set(0.0f, 5.0f);
	player.body = world->CreateBody(&player.bodyDef);

	player.polygonShape.SetAsBox(player.width / 2, player.height / 2);

	player.fixtureDef.shape = &player.polygonShape;
	player.fixtureDef.density = player.density;
	player.fixtureDef.friction = player.friction;
	player.body->CreateFixture(&player.fixtureDef);
}

void LevelManager::SetGroundDefinitions(float x, float y, float width, float height, int r, int g, int b)
{
	Ground new_Ground;
	Position p;
	Shapes s;
	p.pos_x = x;
	p.pos_y = y;
	s.half_width = width / 2;
	s.half_height = height / 2;
	new_Ground.color.r = r;
	new_Ground.color.g = g;
	new_Ground.color.b = b;
	new_Ground.bodyDef.position.Set(p.pos_x, p.pos_y);
	new_Ground.polygonShape.SetAsBox(s.half_width, s.half_height);
	new_Ground.height = s.half_height * 2;
	new_Ground.width = s.half_width * 2;
	list_ground.push_back(new_Ground);
}

void LevelManager::Move(int direction)
{
	switch (direction)
	{
	case 0:
		player.body->SetLinearVelocity(left); // (left, player.body->GetWorldCenter(), true);
		break;
	case 1:
		player.body->SetLinearVelocity(right); //(right, player.body->GetWorldCenter(), true);
		break;
	case 2:
		player.body->ApplyForce(b2Vec2(0.0f, 0.0f), player.body->GetWorldCenter(), true);
		break;
	}
	if (player.body->GetPosition().y > 100)
	{
		player.body->SetTransform(b2Vec2(start_pos), 0);
		ResetGravity();
	}
	else if (player.body->GetPosition().y < -100)
	{
		player.body->SetTransform(b2Vec2(start_pos), 0);
		ResetGravity();
	}
}