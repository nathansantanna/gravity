// C�digo feito por Felipe Rodrigues de Almeida e Nathan Santana
// email: almeidafelipewanted@gmail.com
// website: felipeallmeida.blogspot.com.br
#include <iostream>
#include <string>
#include <GL\glut.h>
#include "GameManager.h"

using namespace std;

GameManager gm;
GLfloat angle, fAspect;
bool loaded = false;
int32 framePeriod = 16;

void Timer(int)
{
	glutPostRedisplay();
	glutTimerFunc(framePeriod, Timer, 0);
}

void Initializer()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	angle = 45;
}

void setViewParameters(void)
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-40 + gm.returnLevelManager().player.body->GetPosition().x, 40 + gm.returnLevelManager().player.body->GetPosition().x, -40 + gm.returnLevelManager().player.body->GetPosition().y, 40 + gm.returnLevelManager().player.body->GetPosition().y);
}

void Drawn(void)
{
	setViewParameters();
	gm.Simulate();
	glClear(GL_COLOR_BUFFER_BIT);
	gm.Drawn();
	glutSwapBuffers();
}


void changeWindowSize(GLsizei w, GLsizei h)
{
	if (h == 0) h = 1;
	glViewport(0, 0, w, h);
	fAspect = (GLfloat)w / (GLfloat)h;
	setViewParameters();
}

void keyboard(unsigned char key, int x, int y)
{

	if (key == 27)
		exit(0);
	if (key == 'a')
		gm.returnLevelManager().Move(0);
	if (key == 'd')
		gm.returnLevelManager().Move(1); 		
	if (key == ' ')
	{
		gm.returnLevelManager().Invert_Gravity();
		gm.returnLevelManager().Move(2);
	}
		
	glutPostRedisplay();
}

int main(int argc, char** argv)
{
	B2_NOT_USED(argc);
	B2_NOT_USED(argv);
	if (!loaded)
	{	
		gm.Initializer();
		loaded = true;
	}
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE| GLUT_RGB);
	glutInitWindowSize(400, 300);
	glutCreateWindow("Game");
	glutKeyboardFunc(keyboard);
	glutDisplayFunc(Drawn);
	glutReshapeFunc(changeWindowSize);
	glutTimerFunc(framePeriod, Timer, 0);
	Initializer();
	glutMainLoop();
}